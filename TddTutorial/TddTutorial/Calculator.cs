﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDTutorial
{
    public interface ICalculator
    {
        int Calculate(string str);
    }
    public class Calculator : ICalculator
    {
        protected string[] spliters = new string[] { "," , "\n"};
        protected string[] separator = new string[] { "][" };
        protected virtual int StringToNum(string value)
        {
            int tmp;
            if (!int.TryParse(value, out tmp))
                throw new ArgumentException("Not a number");
            if (tmp < 0)
                throw new ArgumentException("Number is lower than zero");
            if (tmp > 1000) //grater than 1000 should be ignored
                return 0;
            return tmp;

        }
        public virtual int Calculate(string str)
        {
            if(string.IsNullOrEmpty(str))
                return 0;
            else
            {
                string[] values = str.Split(spliters, StringSplitOptions.None);
                if( values[0].StartsWith("//") )
                {
                    if(values[0][2] != '[')
                        values = str.Replace(values[0].Substring(2, values[0].Length -2), spliters[0] ).Split(spliters, StringSplitOptions.None).Skip(2).ToArray();
                    else if( values[0][2] == '[' && values[0][values[0].Length-1] == ']')
                    {
                        string[] separators = values[0].Substring(3, values[0].Length - 4).Split( separator, StringSplitOptions.None);
                        string tmp = str.Substring(values[0].Length);
                        //
                        foreach( var s in separators)
                            tmp = tmp.Replace(s, spliters[0]);
                        //
                        values = tmp.Split(spliters, StringSplitOptions.None).Skip(1).ToArray();
                    }
                }
                int sum = 0;
                foreach (var value in values)
                {
                    sum += StringToNum(value);
                }
                return sum;
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TDDTutorial;

namespace TDDTutorialTests
{
    [TestClass]
    public class CalculatorTests
    {
        private static ICalculator calculator;
        [ClassInitialize]
        public static void Init(TestContext context)
        {
            calculator = new Calculator();
        }
        [TestMethod]
        public void EmptyStringReturnsZero()
        {
            //Arrange
            string str = null;
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(result, 0);
        }
        [TestMethod]
        public void SingleNumberReturnsValue()
        {
            //Arrange
            string str = "1";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(result, 1);

        }
        [TestMethod]
        public void TwoNumbersReturnSumWithCommaSeparator()
        {
            //Arrange
            string str = "10,32";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(result, 42);
            //Arrange
            string str2 = "0,7";
            //Act
            int result2 = calculator.Calculate(str2);
            //Assert
            Assert.AreEqual(result2, 7);
        }
        [TestMethod]
        public void TwoNumbersReturnSumWithEndLineSeparator()
        {
            //Arrange
            string str = "10\n32";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(result, 42);
            //Arrange
            string str2 = "0\n7";
            //Act
            int result2 = calculator.Calculate(str2);
            //Assert
            Assert.AreEqual(result2, 7);
        }
        [TestMethod]
        public void NumbersWithAnySeparator()
        {
            //Arrange
            string str = "10\n32,3";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(result, 45);
            //Arrange
            string str2 = "0,12\n7";
            //Act
            int result2 = calculator.Calculate(str2);
            //Assert
            Assert.AreEqual(result2, 19);
            //Arrange
            string str3 = "0,127,3,70";
            //Act
            int result3 = calculator.Calculate(str3);
            //Assert
            Assert.AreEqual(result3, 200);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void RecivingMinusOneThrowsException()
        {
            //Arrange
            string str = "10\n32,-1";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.Fail("should never be called");
        }
        [TestMethod]
        public void NumberGraterThan1000AreIgnored()
        {
            //Arrange
            string str = "1000\n32,3";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(result, 1035);
            //Arrange
            string str2 = "10000,12\n7";
            //Act
            int result2 = calculator.Calculate(str2);
            //Assert
            Assert.AreEqual(result2, 19);
            //Arrange
            string str3 = "0,127,3,3270";
            //Act
            int result3 = calculator.Calculate(str3);
            //Assert
            Assert.AreEqual(result3, 130);
            //Arrange
            string str4 = "3270";
            //Act
            int result4 = calculator.Calculate(str4);
            //Assert
            Assert.AreEqual(result4, 0);
        }

        [TestMethod]
        public void SingleDelimiterInFirstLine()
        {
            //Arrange
            string str = "//#\n1000#32,3";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(1035, result);
        }
        [TestMethod]
        public void MultiDelimiterInFirstLine()
        {
            //Arrange
            string str = "//plus\n1000plus32,3";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(1035, result);
        }
        [TestMethod]
        public void MultiOrSingleCharMultipleDelimitersInFirstLine()
        {
            //Arrange
            string str = "//[plus][#]\n1000plus32#3";
            //Act
            int result = calculator.Calculate(str);
            //Assert
            Assert.AreEqual(1035, result);
        }
    }
}
